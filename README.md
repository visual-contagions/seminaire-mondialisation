# Mondialisation, circulation des styles
 <center>Scripts et données du séminaire "mondialisation artistique et culturelle",<br> 2022-2023, UNIGE </center>



<br><br>



Ce répertoire vous donne accès au *notebook* permettant d'analyser le contenu de catalogues d'expositions avec Python - notamment par la réalisation de nuages de mots. Le notebook est lui-même une introduction progressive au langage de programmation `Python`. 


Notre objectif est d'analyser l'évolution progressive des styles des oeuvres, en observant la chose à partir des *titres* des oeuvres mentionnés par les catalogues de la base Artl@s. 

* `1._Catalogue_Nuage_Mots.ipynb`: une première étape vous apprend comment réaliser un nuage de mots à partir des titres, puis des mots les plus fréquents dans un catalogue d'exposition.


* `2._CatalogueS_NuageS-Mots.ipynb` : dans un second temps, on se propose de refaire la même démarche sur plusieurs catalogues de date successives. 
L'intérêt des notebooks et de Python étant d'automatiser des tâches répétitives, l'enjeu est de programmer quelque chose qui, partant de l' `input` constitué par un dossier contenant plusieurs fichiers de catalogues, obtenir en `output` une frise chronologique (datée) de nuages de mots successifs. 

* `3. Basart_evol_titres.ipynb` : le 3e notebook part de la base complète du projet Artlas, et vous propose de réaliser des nuages de mots selon les années et selon la colonne que l'on choisit. L'intéret de ce notebook est qu'il vous apprend notamment à faire des tris dans des dataframes; puis qu'il vous entraîne à utiliser plus souvent des fonctions et des boucles. 

---

Dans le dossier [Tropy](tropy/), vous trouverez le notebook pour exporter les images d'Explore et les analyser à l'aide de [Tropy] (https://tropy.org).


